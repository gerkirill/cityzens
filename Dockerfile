FROM microsoft/aspnetcore:latest

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs

#COPY . /app
COPY ./bin/Debug/netcoreapp1.1/publish /app

WORKDIR /app


EXPOSE 80/tcp
ENTRYPOINT ["dotnet", "cityzens.dll"]
